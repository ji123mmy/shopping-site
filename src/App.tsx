import React from 'react'
import { Navbar } from 'components'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import HOME_ROUTE_LIST from 'route/HomeRoute'

const App: React.FC = () => {
  return (
    <Router>
      <>
        <Navbar />
        <Switch>
          {HOME_ROUTE_LIST.map(route => (
            <Route
              key={route.path}
              exact path={route.path}
              component={route.component} />
          ))}
        </Switch>
      </>
    </Router>
  )
}

export default App
