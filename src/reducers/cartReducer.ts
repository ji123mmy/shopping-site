import types from 'constants/actionTypes'
import initialState from './initialState'
import { CartAction } from './types'

export default (state = initialState, action:CartAction): unknown => {
  switch (action.type) {
    case types.GET_CART_SUCCESS:
    case types.ADD_CART_SUCCESS:
    case types.UPDATE_CART_SUCCESS:
    case types.REMOVE_CART_SUCCESS:
    case types.EMPTY_CART_SUCCESS:
    case types.REFRESH_CART_SUCCESS:
    case types.GET_CART_FAILURE:
    case types.ADD_CART_FAILURE:
    case types.UPDATE_CART_FAILURE:
    case types.REMOVE_CART_FAILURE:
    case types.EMPTY_CART_FAILURE:
    case types.REFRESH_CART_FAILURE:
      return {
        ...state,
        cart: action.payload
      }
    default:
      return {
        ...state
      }
  }
}
