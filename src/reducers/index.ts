import { combineReducers } from 'redux'
import cartReducer from './cartReducer'

const rootReducer = combineReducers({
  common: cartReducer
})
export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
