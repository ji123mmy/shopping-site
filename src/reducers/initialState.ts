import { CartState } from './types'
import { Cart } from '@chec/commerce.js/types/cart'

const intitalState: CartState = {
  cart: {} as Cart
}

export default intitalState
