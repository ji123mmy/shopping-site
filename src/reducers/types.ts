import { Cart } from '@chec/commerce.js/types/cart'

export interface ReduxState {
    common: {
        cart:Cart
    }
}

export interface CartState {
    cart: Cart
}

export interface CartAction {
    type: string
    payload: Record<string, unknown>
}

export interface GetCartPayload {
    type: string
}

export interface GetCartSuccess {
    type: string
    payload: Cart
}

export interface GetCartFailure {
    type: string
    payload: Cart
}

export interface AddCartRequest {
    type: string
}

export interface AddCartPayload {
    type: string
    payload: {
        productId: string,
        quantity: number
    }
}

export interface AddCartSuccess {
    type: string
    payload: Cart
}

export interface AddCartFailure {
    type: string
    payload: boolean
}

export interface UpdateCartPayload {
    type: string
    payload: {
        productId: string,
        quantity: number
    }
}

export interface UpdateCartSuccess {
    type: string
    payload: Cart
}

export interface UpdateCartFailure {
    type: string
    payload: boolean
}

export interface RemoveCartPayload {
    type: string
    payload: { productId: string }
}

export interface RemoveCartSuccess {
    type: string
    payload: Cart
}

export interface RemoveCartFailure {
    type: string
    payload: boolean
}

export interface EmptyCartPayload {
    type: string
}

export interface EmptyCartSuccess {
    type: string
    payload: Cart
}

export interface EmptyCartFailure {
    type: string
    payload: boolean
}

export interface RefreshCartPayload {
    type: string
}

export interface RefreshCartSuccess {
    type: string
    payload: Cart
}

export interface RefreshCartFailure {
    type: string
    payload: Cart
}
