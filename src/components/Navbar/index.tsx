import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { ReduxState } from 'reducers/types'
import { AppBar, Toolbar, IconButton, Badge, Typography } from '@material-ui/core'
import { ShoppingCart } from '@material-ui/icons'
import { Link, useLocation } from 'react-router-dom'
import { getCart } from 'actions/cartAction'

import useStyles from './styles'
import logo from 'assets/commerce.png'

const Navbar: React.FC = React.memo(() => {
  const cart = useSelector((state:ReduxState) => state.common.cart)
  const classes = useStyles()
  const location = useLocation()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getCart())
  }, [dispatch])

  return (
    <>
      <AppBar position='fixed' className={classes.appBar} color='inherit'>
        <Toolbar>
          <>
            <Typography component={Link} to='/' variant='h2'>
              <img src={logo} alt='' height='25px' className={classes.image} />
            </Typography>
            <Typography className={classes.title} component={Link} to='/' variant='h5'>
              Commerce.js
            </Typography>
          </>
          <div className={classes.grow} />
          {location.pathname !== '/cart' && (
            <div>
              <IconButton component={Link} to='/cart' aria-label='Show cart items'>
                <Badge badgeContent={cart.total_items} color='secondary'>
                  <ShoppingCart />
                </Badge>
              </IconButton>
            </div>
          )}
        </Toolbar>
      </AppBar>
    </>
  )
})

Navbar.displayName = 'Navbar'

export default Navbar
