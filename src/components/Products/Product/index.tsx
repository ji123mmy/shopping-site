import React, { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { addToCart } from 'actions/cartAction'
import { Card, CardMedia, CardContent, CardActions, Typography, IconButton } from '@material-ui/core'
import { AddShoppingCart } from '@material-ui/icons'
import useStyle from './styles'
import { Product as ProductData } from '@chec/commerce.js/types/product'

const Product: React.FC<{ product: ProductData }> = ({ product }) => {
  const classes = useStyle()
  const dispatch = useDispatch()

  const handleAddToCart:(productId: string, quantity: number) => Promise<void> = useCallback(async (productId: string, quantity: number) => {
    dispatch(addToCart(productId, quantity))
  }, [dispatch])

  return (
    <Card className={classes.root}>
      <CardMedia className={classes.media} title={product.name} image={product.media.source} />
      <CardContent>
        <div className={classes.cardContent}>
          <Typography variant='h5' gutterBottom>
            {product.name}
          </Typography>
          <Typography variant='h5'>
            {product.price.formatted_with_symbol}
          </Typography>
        </div>
        <Typography dangerouslySetInnerHTML={{ __html: product.description }} variant='body2' color='textSecondary' />
      </CardContent>
      <CardActions disableSpacing className={classes.cardActions}>
        <IconButton onClick={() => handleAddToCart(product.id, 1)} aria-label='Add to Cart'>
          <AddShoppingCart />
        </IconButton>
      </CardActions>
    </Card>
  )
}

Product.displayName = 'Product'

export default Product
