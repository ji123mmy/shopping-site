import React, { useState, useEffect } from 'react'
import { Grid } from '@material-ui/core'
import Product from './Product'
import CircularProgress from '@material-ui/core/CircularProgress'
import { commerce } from 'lib/commerce'

import useStyles from './style'
import { Product as productData } from '@chec/commerce.js/types/product'

const Products: React.FC = React.memo(() => {
  const classes = useStyles()
  const [products, setProducts] = useState<productData[]>([])

  useEffect(() => {
    const fetchProducts = async () => {
      const { data } = await commerce.products.list()
      setProducts(data)
    }
    fetchProducts()
  }, [])

  if (products.length === 0) return <div className={classes.spinner}><CircularProgress /> </div>

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Grid container justifyContent='center' spacing={4}>
        {products.map((product: productData) => (
          <Grid item key={product.id} xs={12} sm={6} md={4} lg={3}>
            <Product product={product} />
          </Grid>
        ))}
      </Grid>
    </main>
  )
})

Products.displayName = 'Products'

export default Products
