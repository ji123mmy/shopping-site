export interface shippingData {
    firstName: string | undefined
    lastName: string | undefined
    email: string
    address1: string
    City: string
    shippingSubdivision: string
    zip: string
    shippingCountry: string
    shippingOption: string
}
