import React, { useState, useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'
import { ReduxState } from 'reducers/types'
import { shippingData } from '../types'
import { CheckoutToken } from '@chec/commerce.js/types/checkout-token'
import { Paper, Stepper, Step, StepLabel, Typography, CircularProgress, Divider, Button, CssBaseline } from '@material-ui/core'
import { commerce } from 'lib/commerce'
import { refreshCart } from 'actions/cartAction'
import AddressForm from '../AddressForm'
import PaymentForm from '../PaymentForm'
import useStyles from './styles'
import { CheckoutCapture } from '@chec/commerce.js/types/checkout-capture'
import { CheckoutCaptureResponse } from '@chec/commerce.js/types/checkout-capture-response'

const steps = ['Shipping address', 'Payment details']

const Checkout: React.FC = React.memo(() => {
  const [activeStep, setActiveStep] = useState<number>(0)
  const [shippingData, setShippingData] = useState<shippingData>({} as shippingData)
  const [checkoutToken, setCheckoutToken] = useState<CheckoutToken>({} as CheckoutToken)
  const [order, setOrder] = useState<CheckoutCaptureResponse>({} as CheckoutCaptureResponse)
  const [isFinished, setIsFinished] = useState<boolean>(false)
  const cart = useSelector((state:ReduxState) => state.common.cart)
  const classes = useStyles()
  const dispatch = useDispatch()
  const history = useHistory()

  const next = useCallback((data) => {
    setShippingData(data)
    setActiveStep(prev => prev + 1)
  }, [])

  const handlebackStep = useCallback(() => {
    setActiveStep(prev => prev - 1)
  }, [])

  const handleSendOrder = useCallback(async (orderData:CheckoutCapture) => {
    setActiveStep(prev => prev + 1)
    setTimeout(() => setIsFinished(true), 3000)
    const incompingOrder = await commerce.checkout.capture(checkoutToken.id, orderData)
    setOrder(incompingOrder)
    dispatch(refreshCart())
  }, [checkoutToken, dispatch])

  useEffect(() => {
    const generatToken = async() => {
      try {
        const token:CheckoutToken = await commerce.checkout.generateToken(cart.id, { type: 'cart' })
        setCheckoutToken(token)
      } catch (err) {
        history.push('/')
      }
    }
    generatToken()
  }, [cart, history])

  const Confirmation: React.FC = () => order.customer ? (
    <>
      <div>
        <Typography variant='h5'> Thank you form your purchase, {order.customer.firstname} {order.customer.lastname} </Typography>
        <Divider className={classes.divider} />
        <Typography variant='subtitle2'> Order ref: {order.customer_reference} </Typography>
      </div>
      <br />
      <Button component={Link} to='/' variant='outlined' type='button'>Back to Home</Button>
    </>
  ) : isFinished ? (
    <>
      <div>
        <Typography variant='h5'> Thank you form your purchase.</Typography>
        <Divider className={classes.divider} />
      </div>
      <br />
      <Button component={Link} to='/' variant='outlined' type='button'>Back to Home</Button>
    </>
  ) : (
    <div className={classes.spinner}>
      <CircularProgress />
    </div>
  )

  const Form: React.FC = () => activeStep === 0
    ? <AddressForm checkoutToken={checkoutToken} next={next} />
    : <PaymentForm shippingData={shippingData} checkoutToken={checkoutToken} backStep={handlebackStep} sendOrder={handleSendOrder} />
  return (
    <>
      <CssBaseline />
      <div className={classes.toolbar} />
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Typography variant='h4' align='center'>Checkout</Typography>
          <Stepper activeStep={activeStep} className={classes.stepper}>
            {steps.map(step => (
              <Step key={step}>
                <StepLabel>{step}</StepLabel>
              </Step>
            ))}
          </Stepper>
          {activeStep === steps.length ? <Confirmation /> : checkoutToken && <Form />}
        </Paper>
      </main>
    </>
  )
})

Checkout.displayName = 'Checkout'

export default Checkout
