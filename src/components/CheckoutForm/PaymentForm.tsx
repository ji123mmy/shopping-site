import React, { useCallback } from 'react'
import { CheckoutToken } from '@chec/commerce.js/types/checkout-token'
import { Typography, Button, Divider } from '@material-ui/core'
import { Elements, CardElement, ElementsConsumer } from '@stripe/react-stripe-js'
import { loadStripe, Stripe, StripeElements } from '@stripe/stripe-js'
import { shippingData } from './types'
import Review from './Review'
import { CheckoutCapture } from '@chec/commerce.js/types/checkout-capture'

const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PUBLIC_KEY || '')

const PaymentForm: React.FC<{ shippingData: shippingData, checkoutToken: CheckoutToken, backStep:() => void, sendOrder: (orderData:CheckoutCapture) => void }> =
({ shippingData, checkoutToken, backStep, sendOrder }) => {

  const handleSubmit = useCallback(async (event:React.FormEvent<HTMLFormElement>, elements: StripeElements | null, stripe: Stripe | null) => {
    event.preventDefault()
    if (!stripe || !elements) return
    const orderData = {
      line_items: checkoutToken.live.line_items,
      customer: { firstname: shippingData.firstName || '', lastname: shippingData.lastName, email: shippingData.email },
      shipping: {
        name: 'Primary',
        street: shippingData.address1,
        town_city: shippingData.City,
        county_state: shippingData.shippingSubdivision,
        postal_zip_code: shippingData.zip,
        country: shippingData.shippingCountry
      },
      fulfillment: { shipping_method: shippingData.shippingOption },
      payment: {
        gateway: 'gway_4oG3G0DGRGXKo6',
      }
    }
    try {
      sendOrder(orderData)
    } catch (error) {
      console.log(error)
    }

  }, [checkoutToken, sendOrder, shippingData])

  return (
    <>
      <Review checkoutToken={checkoutToken} />
      <Divider />
      <Typography variant='h6' gutterBottom style={{ margin: '20px 0' }}>Payment method</Typography>
      <Elements stripe={stripePromise}>
        <ElementsConsumer>
          {({ elements, stripe }) => (
            <form onSubmit={(e) => handleSubmit(e, elements, stripe)}>
              <CardElement />
              <br /> <br />
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <Button variant='outlined' onClick={() => backStep()}>Back</Button>
                <Button type='submit' variant='contained' disabled={!stripe} color='primary'>
                  Pay {checkoutToken.live.subtotal.formatted_with_symbol}
                </Button>
              </div>
            </form>
          )}
        </ElementsConsumer>
      </Elements>
    </>
  )
}

export default PaymentForm
