import React, { useState, useEffect, useMemo } from 'react'
import { Link } from 'react-router-dom'
import { CheckoutToken } from '@chec/commerce.js/types/checkout-token'
import { GetShippingOptionsResponse as OptionsResponse } from '@chec/commerce.js/features/checkout'
import { InputLabel, Select, MenuItem, Button, Grid, Typography, CircularProgress } from '@material-ui/core'
import { useForm, FormProvider } from 'react-hook-form'
import FormInput from './CustomTextFild'
import { commerce } from 'lib/commerce'

const AddressForm: React.FC<{ checkoutToken: CheckoutToken, next: (data:Record<string, unknown>) => void }> = ({ checkoutToken, next }) => {
  const [shippingCountries, setShippingCountries] = useState<{ [name: string]: string }>({})
  const [shippingCountry, setShippingCountry] = useState<string>('')
  const [shippingSubdivisions, setShippingSubdivisions] = useState<{ [name: string]: string }>({})
  const [shippingSubdivision, setShippingSubdivision] = useState<string>('')
  const [shippingOptions, setShippingOptions] = useState([])
  const [shippingOption, setShippingOption] = useState<string>('')
  const methods = useForm()

  const countries = useMemo(() => {
    return Object.entries(shippingCountries).map(([code, name]:string[]) => ({ id: code, label: name }))
  }, [shippingCountries])

  const subdivisions = useMemo(() => {
    return Object.entries(shippingSubdivisions).map(([code, name]:string[]) => ({ id: code, label: name }))
  }, [shippingSubdivisions])

  const options = useMemo(() => {
    return shippingOptions.map((sO:OptionsResponse) => ({ id: sO.id, label: `${sO.description}-(${sO.price.formatted_with_symbol})` }))
  }, [shippingOptions])

  useEffect(() => {
    const fetchShippingCountries = async (checkoutTokenId:string) => {
      if (!checkoutTokenId) return
      const { countries } = await commerce.services.localeListShippingCountries(checkoutTokenId)
      setShippingCountries(countries)
      setShippingCountry(Object.keys(countries)[0])
    }
    fetchShippingCountries(checkoutToken.id)
  }, [checkoutToken])

  useEffect(() => {
    const fetchSubdivisions = async (countryCode:string) => {
      const { subdivisions } = await commerce.services.localeListSubdivisions(countryCode)
      setShippingSubdivisions(subdivisions)
      setShippingSubdivision(Object.keys(subdivisions)[0])
    }
    if (shippingCountry) fetchSubdivisions(shippingCountry)
  }, [shippingCountry])

  useEffect(() => {
    const fetchShippingOptions = async (checkoutTokenId:string, country:string, region = '') => {
      const options: any = await commerce.checkout.getShippingOptions(checkoutTokenId, { country, region })
      setShippingOptions(options)
      setShippingOption(options[0].id)
    }
    if (shippingCountry && shippingSubdivision) fetchShippingOptions(checkoutToken.id, shippingCountry, shippingSubdivision)
  }, [checkoutToken, shippingCountry, shippingSubdivision])

  if (!shippingCountry || !shippingSubdivision || !shippingOption) {
    return (
      <div style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <CircularProgress />
      </div>
    )
  }

  return (
    <>
      <Typography variant='h6' gutterBottom>Shipping Address</Typography>
      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit(data => next({ ...data, shippingCountry, shippingSubdivision, shippingOption }))}>
          <Grid container spacing={3}>
            <FormInput name='firstName' label='First name' />
            <FormInput name='lastName' label='Last name' />
            <FormInput name='address1' label='Address' />
            <FormInput name='email' label='Email' />
            <FormInput name='City' label='City' />
            <FormInput name='zip' label='ZIP / Postal code' />
            <Grid item xs={12} sm={6}>
              <InputLabel>Shipping Country </InputLabel>
              <Select
                value={shippingCountry} fullWidth onChange={e => {
                  if (typeof e.target.value === 'string') setShippingCountry(e.target.value)
                }}>
                {countries.map(({ id, label }) => (
                  <MenuItem key={id} value={id}>
                    {label}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
            <Grid item xs={12} sm={6}>
              <InputLabel>Shipping Subdivision </InputLabel>
              <Select
                value={shippingSubdivision} fullWidth onChange={e => {
                  if (typeof e.target.value === 'string') setShippingSubdivision(e.target.value)
                }}>
                {subdivisions.map(({ id, label }) => (
                  <MenuItem key={id} value={id}>
                    {label}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
            <Grid item xs={12} sm={6}>
              <InputLabel>Shipping Options </InputLabel>
              <Select
                value={shippingOption} fullWidth onChange={e => {
                  if (typeof e.target.value === 'string') setShippingOption(e.target.value)
                }}>
                {options.map(({ id, label }) => (
                  <MenuItem key={id} value={id}>
                    {label}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
          </Grid>
          <div style={{ display: 'flex', justifyContent: 'space-between', margin: '10px 0' }}>
            <Button variant='outlined'>
              <Link style={{ color: 'black', textDecoration: 'none' }} to='/cart'>Back to Cart</Link>
            </Button>
            <Button type='submit' variant='contained' color='primary'>Next</Button>
          </div>
        </form>
      </FormProvider>
    </>
  )
}

export default AddressForm
