import React from 'react'
import { TextField, Grid } from '@material-ui/core'
import { useFormContext, Controller } from 'react-hook-form'

const FormInput: React.FC<{ name:string, label:string}> = ({ name, label }) => {
  const { control } = useFormContext()

  return (
    <Grid item xs={12} sm={6}>
      <Controller
        name={name}
        control={control}
        rules={{ required: true }}
        defaultValue=''
        render={({ field }) =>
          <TextField
            fullWidth
            label={label}
            {...field} />} />
    </Grid>
  )
}

export default FormInput
