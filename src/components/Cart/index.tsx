import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { ReduxState } from 'reducers/types'
import { getCart, emptyCart } from 'actions/cartAction'
import { Container, Typography, Button, Grid } from '@material-ui/core'
import CircularProgress from '@material-ui/core/CircularProgress'
import { Link } from 'react-router-dom'

import CartItem from './CartItem'
import useStyles from './styles'

const Cart: React.FC = React.memo(() => {
  const cart = useSelector((state:ReduxState) => state.common.cart)
  const classes = useStyles()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getCart())
  }, [dispatch])

  const EmptyCart: React.FC = () => (
    <Typography variant='subtitle1'>You have no items in your shopping cart.
      <Link to='/' className={classes.link}>start adding some!</Link>
    </Typography>
  )

  const FilterCart: React.FC = () => (
    <>
      <Grid container spacing={3}>
        {cart.line_items.map(item => (
          <Grid item xs={12} sm={4} key={item.id}>
            <CartItem item={item} />
          </Grid>
        ))}
      </Grid>
      <div className={classes.cardDetails}>
        <Typography variant='h4'>Subtotal: {cart.subtotal.formatted_with_symbol}</Typography>
        <div>
          <Button
            className={classes.emptyButton}
            size='large' type='button'
            variant='contained'
            color='secondary'
            onClick={() => dispatch(emptyCart())}>
            Empty Cart
          </Button>
          <Button
            className={classes.checkoutButton}
            component={Link}
            to='/checkout'
            size='large'
            type='button'
            variant='contained'
            color='primary'>
            Check out
          </Button>
        </div>
      </div>
    </>
  )

  if (!cart.line_items) return <div className={classes.loadingIcon}><CircularProgress /> </div>

  return (
    <Container>
      <div className={classes.toolbar} />
      <Typography className={classes.title} variant='h3' gutterBottom>Your Shopping Cart</Typography>
      {!cart.line_items?.length ? <EmptyCart /> : <FilterCart />}
    </Container>
  )
})

Cart.displayName = 'Cart'

export default Cart
