import React from 'react'
import { useDispatch } from 'react-redux'
import { Typography, Button, Card, CardActions, CardContent, CardMedia } from '@material-ui/core'
import { LineItem } from '@chec/commerce.js/types/line-item'

import useStyles from './styles'
import { removeCart, updateCart } from 'actions/cartAction'

const CartItem: React.FC <{ item: LineItem }> = ({ item }) => {
  const classes = useStyles()
  const dispatch = useDispatch()
  return (
    <Card>
      <CardMedia image={item.media.source} className={classes.media} />
      <CardContent className={classes.cardContent}>
        <Typography variant='h4'>{item.name}</Typography>
        <Typography variant='h4'>{item.line_total.formatted_with_symbol}</Typography>
        <CardActions className={classes.cardActions}>
          <div className={classes.buttons}>
            <Button type='button' size='small' onClick={() => dispatch(updateCart(item.id, item.quantity - 1))}>-</Button>
            <Typography>{item.quantity}</Typography>
            <Button type='button' size='small' onClick={() => dispatch(updateCart(item.id, item.quantity + 1))}>+</Button>
          </div>
          <Button variant='contained' type='button' color='secondary' onClick={() => dispatch(removeCart(item.id))}>Remove</Button>
        </CardActions>
      </CardContent>
    </Card>
  )
}

export default CartItem
