import { Products, Cart, Checkout } from 'components'

const HOME_ROUTE_LIST = [
  {
    path: '/',
    component: Products
  }, {
    path: '/cart',
    component: Cart
  },
  {
    path: '/checkout',
    component: Checkout
  }
]

export default HOME_ROUTE_LIST
