import types from 'constants/actionTypes'

export function getCart() {
  return {
    type: types.GET_CART_SAGA,
  }
}

export function addToCart(productId: string, quantity: number) {
  return {
    type: types.ADD_CART_SAGA,
    payload: { productId, quantity }
  }
}

export function updateCart(productId: string, quantity: number) {
  return {
    type: types.UPDATE_CART_SAGA,
    payload: { productId, quantity }
  }
}
export function removeCart(productId: string) {
  return {
    type: types.REMOVE_CART_SAGA,
    payload: { productId }
  }
}

export function emptyCart() {
  return {
    type: types.EMPTY_CART_SAGA,
  }
}

export function refreshCart() {
  return {
    type: types.REFRESH_CART_SAGA,
  }
}
