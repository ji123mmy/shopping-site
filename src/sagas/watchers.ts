import { takeLatest, all } from 'redux-saga/effects'
import { addCartSaga, emptyCartSaga, getCartSaga, refreshCartSaga, removeCartSaga, updateCartSaga } from './cartSaga'
import types from 'constants/actionTypes'

export function* initialWebAppSaga() {
  yield all([
    takeLatest(types.ADD_CART_SAGA, addCartSaga),
    takeLatest(types.GET_CART_SAGA, getCartSaga),
    takeLatest(types.UPDATE_CART_SAGA, updateCartSaga),
    takeLatest(types.REMOVE_CART_SAGA, removeCartSaga),
    takeLatest(types.EMPTY_CART_SAGA, emptyCartSaga),
    takeLatest(types.REFRESH_CART_SAGA, refreshCartSaga)
  ])
}
