import { fork, all } from 'redux-saga/effects'
import { initialWebAppSaga } from './watchers'

export default function* rootSaga() {
  yield all([fork(initialWebAppSaga)])
}
