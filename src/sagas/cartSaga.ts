import { commerce } from 'lib/commerce'
import { Cart } from '@chec/commerce.js/types/cart'
import { AddUpdateResponse, EmptyResponse } from '@chec/commerce.js/features/cart'

import {
  AddCartPayload,
  AddCartSuccess,
  AddCartFailure,
  GetCartPayload,
  GetCartSuccess,
  GetCartFailure,
  UpdateCartPayload,
  UpdateCartSuccess,
  UpdateCartFailure,
  RemoveCartSuccess,
  RemoveCartFailure,
  RemoveCartPayload,
  EmptyCartPayload,
  EmptyCartSuccess,
  EmptyCartFailure,
  RefreshCartSuccess,
  RefreshCartPayload,
  RefreshCartFailure
} from 'reducers/types'
import { call, put } from 'redux-saga/effects'
import types from 'constants/actionTypes'

export function* getCartSaga ({ type }: GetCartPayload) {
  try {
    const result: Cart = yield call(() => commerce.cart.retrieve())
    yield put(
      result.id ? getCartSuccess(result) : getCartFailure(result)
    )
  } catch (e) {
    yield put({ type, payload: e })
  }
}

export const getCartSuccess = (payload: Cart): GetCartSuccess => ({
  type: types.GET_CART_SUCCESS,
  payload
})

export const getCartFailure = (payload: Cart): GetCartFailure => ({
  type: types.GET_CART_FAILURE,
  payload
})

export function* addCartSaga ({ type, payload }:AddCartPayload) {
  try {
    const result: AddUpdateResponse = yield call(() => commerce.cart.add(payload.productId, payload.quantity))
    yield put(
      result.success ? addCartSuccess(result.cart) : addCartFailure(result.success)
    )
  } catch (e) {
    yield put({ type, payload: e })
  }
}

export const addCartSuccess = (payload: Cart): AddCartSuccess => ({
  type: types.ADD_CART_SUCCESS,
  payload
})

export const addCartFailure = (payload: boolean): AddCartFailure => ({
  type: types.ADD_CART_FAILURE,
  payload
})

export function* updateCartSaga ({ type, payload }: UpdateCartPayload) {
  try {
    const result: AddUpdateResponse = yield call(() => commerce.cart.update(payload.productId, { quantity: payload.quantity }))
    yield put(
      result.success ? updateCartSuccess(result.cart) : updateCartFailure(result.success)
    )
  } catch (e) {
    yield put({ type, payload: e })
  }
}

export const updateCartSuccess = (payload: Cart): UpdateCartSuccess => ({
  type: types.UPDATE_CART_SUCCESS,
  payload
})

export const updateCartFailure = (payload: boolean): UpdateCartFailure => ({
  type: types.UPDATE_CART_FAILURE,
  payload
})

export function* removeCartSaga ({ type, payload }: RemoveCartPayload) {
  try {
    const result: AddUpdateResponse = yield call(() => commerce.cart.remove(payload.productId))
    yield put(
      result.success ? removeCartSuccess(result.cart) : removeCartFailure(result.success)
    )
  } catch (e) {
    yield put({ type, payload: e })
  }
}

export const removeCartSuccess = (payload: Cart): RemoveCartSuccess => ({
  type: types.REMOVE_CART_SUCCESS,
  payload
})

export const removeCartFailure = (payload: boolean): RemoveCartFailure => ({
  type: types.REMOVE_CART_FAILURE,
  payload
})

export function* emptyCartSaga ({ type }: EmptyCartPayload) {
  try {
    const result: EmptyResponse = yield call(() => commerce.cart.empty())
    yield put(
      result.success ? emptyCartSuccess(result.cart) : emptyCartFailure(result.success)
    )
  } catch (e) {
    yield put({ type, payload: e })
  }
}

export const emptyCartSuccess = (payload: Cart): EmptyCartSuccess => ({
  type: types.EMPTY_CART_SUCCESS,
  payload
})

export const emptyCartFailure = (payload: boolean): EmptyCartFailure => ({
  type: types.EMPTY_CART_FAILURE,
  payload
})

export function* refreshCartSaga ({ type }: RefreshCartPayload) {
  try {
    const result: Cart = yield call(() => commerce.cart.refresh())
    yield put(
      result ? refreshCartSuccess(result) : refreshCartFailure(result)
    )
  } catch (e) {
    yield put({ type, payload: e })
  }
}

export const refreshCartSuccess = (payload: Cart): RefreshCartSuccess => ({
  type: types.REFRESH_CART_SUCCESS,
  payload
})

export const refreshCartFailure = (payload: Cart): RefreshCartFailure => ({
  type: types.REFRESH_CART_FAILURE,
  payload
})
