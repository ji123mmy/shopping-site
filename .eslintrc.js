module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'standard',
    'standard-react',
    'eslint:recommended',
    'plugin:react-hooks/recommended',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 12
  },
  plugins: [
    'react',
    'react-hooks',
    '@typescript-eslint'
  ],
  rules: {
    'comma-dangle': 'off',
    'generator-star-spacing': 'off',
    'no-underscore-dangle': 'off',
    'array-callback-return': 'off',
    'space-before-function-paren': 'off',
    camelcase: 'off',
    'padded-blocks': 'off',
    'no-console': 'off',
    'object-property-newline': 'off',
    'max-len': ['error', { code: 200, tabWidth: 2 }],
    'react/prop-types': 0,
    'react/jsx-closing-bracket-location': [1, { selfClosing: 'after-props', nonEmpty: 'after-props' }],
    'no-use-before-define': 'off',
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    '@typescript-eslint/explicit-module-boundary-types': 'off'
  }
}
